import React from 'react';
import Field from './Field';
import Button from './Button';

const UserCreator = () => {
  return(
    <div className="ui form">
      <Field />
      <Button />
    </div>
  );
};

export default UserCreator;
