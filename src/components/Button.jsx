import React from 'react'; 
import LanguagesContext from '../contexts/LanguagesContext'

class Button extends React.Component{
  static contextType = LanguagesContext;
  render(){
    return(
      <button className="ui button primary">
       <LanguagesContext.Consumer>
         {(value) => value === 'english' ? 'Submit' : 'enviar' }
       </LanguagesContext.Consumer>
      </button>
    );
  }
}

export default Button;