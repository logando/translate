import React from 'react';
import UserCreator from './UserCreator';
import LanguagesContext from '../contexts/LanguagesContext';

class App extends React.Component{
  state = {language: 'english'}
  
  onChangeLanguage = language => {
    this.setState({language});
  };

  render(){
    return (
      <div className="ui container">
        <div>
          Select a language:
          <i className="flag us" onClick={()=> this.onChangeLanguage('english')}/>
          <i className="flag do" onClick={()=> this.onChangeLanguage('espanol')}/>
        </div>
        <LanguagesContext.Provider value={this.state.language}>
          <UserCreator/>
        </LanguagesContext.Provider>
       </div>
    );
  }
}

export default App;