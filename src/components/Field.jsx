import React from 'react';
import LanguagesContext from '../contexts/LanguagesContext'

class Field extends React.Component{
  static contextType = LanguagesContext;
  
  render(){
    const text = this.context === 'english' ? 'Name' : 'Nombre'; 
    return(
      <div className="ui field">
        <label>{text}</label>
        <input/>
      </div>
    );
  }
}

export default Field;